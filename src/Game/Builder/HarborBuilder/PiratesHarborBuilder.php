<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class PiratesHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::PIRATES_HARBOR;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::PIRATE_SHIP;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['north', 'south', 'west'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}