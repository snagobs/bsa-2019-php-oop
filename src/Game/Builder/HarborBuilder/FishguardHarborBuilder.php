<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class FishguardHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::FISHGUARD;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::ROYAL_PATROOL_SCHOONER;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['north', 'south', 'east'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}