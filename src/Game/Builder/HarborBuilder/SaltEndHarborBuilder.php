<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class SaltEndHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::SALT_END;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::ROYAL_PATROOL_SCHOONER;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['west', 'south', 'east'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}