<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

class HarborDirector
{
    public function build(HarborBuilderInterface $builder)
    {
        $builder->setHarborName();
        $builder->setShipNameInHarbor();
        $builder->setAvailableDirections();

        return $builder->getHarbor();
    }
}