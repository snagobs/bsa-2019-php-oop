<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

class Harbor
{
    public $harborName;
    public $shipNameInHarbor;
    public $availableDirections = [];

    public function __construct()
    {

    }
}