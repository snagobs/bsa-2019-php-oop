<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class GraysHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::GRAYS;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::ROYAL_BATTLE_SHIP;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['south', 'west'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}