<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class LondonDocksHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::LONDON_DOCKS;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::HMS_ROYAL_SOVEREIGN;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['north', 'west'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}