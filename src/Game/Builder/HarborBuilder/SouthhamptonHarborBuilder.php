<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class SouthhamptonHarborBuilder implements HarborBuilderInterface
{
    private $harbor;

    public function __construct(Harbor $harbor)
    {
        $this->harbor = $harbor;
    }

    public function setHarborName()
    {
        $this->harbor->harborName = Constants::SOUTHHAMPTON;
    }

    public function setShipNameInHarbor()
    {
        $this->harbor->shipNameInHarbor = Constants::ROYAL_PATROOL_SCHOONER;
    }

    public function setAvailableDirections()
    {
        $this->harbor->availableDirections = ['north', 'west', 'east'];
    }

    public function getHarbor()
    {
        return $this->harbor;
    }
}