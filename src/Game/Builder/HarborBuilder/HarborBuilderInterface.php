<?php

namespace BinaryStudioAcademy\Game\Builder\HarborBuilder;

interface HarborBuilderInterface
{
    public function setHarborName();

    public function setShipNameInHarbor();

    public function setAvailableDirections();

    public function getHarbor();
}