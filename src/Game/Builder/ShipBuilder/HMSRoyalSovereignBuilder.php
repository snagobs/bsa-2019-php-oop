<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class HMSRoyalSovereignBuilder implements ShipBuilderInterface
{
    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function setName()
    {
        $this->ship->name = Constants::HMS_ROYAL_SOVEREIGN;
    }

    public function setStrength()
    {
        $this->ship->strength = 10;
    }

    public function setArmour()
    {
        $this->ship->armour = 10;
    }

    public function setLuck()
    {
        $this->ship->luck = 10;
    }

    public function setHealth()
    {
        $this->ship->health = 100;
    }

    public function setHold()
    {
        $this->ship->hold = '[💰 💰 🍾]';
    }

    public function getShip()
    {
        return $this->ship;
    }
}