<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class RoyalPatroolSchoonerBuilder implements ShipBuilderInterface
{
    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function setName()
    {
        $this->ship->name = Constants::ROYAL_PATROOL_SCHOONER;
    }

    public function setStrength()
    {
        $this->ship->strength = 4;
    }

    public function setArmour()
    {
        $this->ship->armour = 4;
    }

    public function setLuck()
    {
        $this->ship->luck = 4;
    }

    public function setHealth()
    {
        $this->ship->health = 50;
    }

    public function setHold()
    {
        $this->ship->hold = '[ 💰 _ _ ]';
    }

    public function getShip()
    {
        return $this->ship;
    }
}