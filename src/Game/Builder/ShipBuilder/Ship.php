<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

class Ship
{
    public $name;
    public $strength;
    public $armour;
    public $luck;
    public $health;
    public $hold;

    public function __construct()
    {

    }
}