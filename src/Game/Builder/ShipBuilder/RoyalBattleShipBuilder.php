<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

class RoyalBattleShipBuilder implements ShipBuilderInterface
{
    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function setName()
    {
        $this->ship->name = Constants::ROYAL_BATTLE_SHIP;
    }

    public function setStrength()
    {
        $this->ship->strength = 8;
    }

    public function setArmour()
    {
        $this->ship->armour = 8;
    }

    public function setLuck()
    {
        $this->ship->luck = 7;
    }

    public function setHealth()
    {
        $this->ship->health = 80;
    }

    public function setHold()
    {
        $this->ship->hold = '[🍾 _ _ ]';
    }

    public function getShip()
    {
        return $this->ship;
    }
}