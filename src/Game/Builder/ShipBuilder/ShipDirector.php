<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

class ShipDirector
{
    public function build(ShipBuilderInterface $builder)
    {
        $builder->setName();
        $builder->setStrength();
        $builder->setArmour();
        $builder->setLuck();
        $builder->setHealth();
        $builder->setHold();

        return $builder->getShip();
    }
}