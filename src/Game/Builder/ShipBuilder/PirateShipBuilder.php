<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

use BinaryStudioAcademy\Game\Helpers\Constants;

Class PirateShipBuilder implements ShipBuilderInterface
{
    private $ship;

    public function __construct(Ship $ship)
    {
        $this->ship = $ship;
    }

    public function setName()
    {
        $this->ship->name = Constants::PIRATE_SHIP;
    }

    public function setStrength()
    {
        $this->ship->strength = 4;
    }

    public function setArmour()
    {
        $this->ship->armour = 4;
    }

    public function setLuck()
    {
        $this->ship->luck = 4;
    }

    public function setHealth()
    {
        $this->ship->health = 60;
    }

    public function setHold()
    {
        $this->ship->hold = '[_ _ _]';
    }

    public function getShip()
    {
        return $this->ship;
    }
}