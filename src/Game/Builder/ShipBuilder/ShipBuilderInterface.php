<?php

namespace BinaryStudioAcademy\Game\Builder\ShipBuilder;

interface ShipBuilderInterface
{
    public function setName();

    public function setStrength();

    public function setArmour();

    public function setLuck();

    public function setHealth();

    public function setHold();

    public function getShip();
}