<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Command\CreateHarbors;
use BinaryStudioAcademy\Game\Command\CreateShips;
use BinaryStudioAcademy\Game\Command\Messages;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    private $ship;
    private $harbor;
    private $messages;

    public function __construct(Random $random)
    {
        $this->random = $random;

    }

    public function start(Reader $reader, Writer $writer, Messages $messages, CreateShips $ships, CreateHarbors $harbors)
    {
        $this->harbor = $harbors->createHarbor();
        $this->ship = $ships->createShips();
        $this->messages = $messages;

        $writer->writeln('Welcome to the game "Battle Ship"!!!');
        $writer->writeln('Your goal is to destroy all enemy ships, collect all the gold and rum and become the strongest in the whole game world!');
        $writer->writeln('Press enter to start... ');
        $input = trim($reader->read());
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('Please, enter your command, Big Pirate Boss ;) ');

        while (true) {
            switch ($input = trim($reader->read())) {
                case 'help' :
                    $writer->writeln($this->messages->help());
                    $writer->writeln('Please, enter your command, Big Pirates Boss ;) ');
                    break;
                case 'stats' :
                    $writer->writeln($this->messages->stats($this->ship['player']));
                    $writer->writeln('Please, enter your command, Big Pirates Boss ;) ');
                    break;

                default:
                    break;
            }
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
        $writer->writeln('Please, enter your command, Big Pirates Boss ;) ');
    }
}
