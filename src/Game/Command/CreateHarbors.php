<?php


namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\HarborBuilder\FelixtoweHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\FishguardHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\GraysHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\Harbor;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\HarborDirector;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\IsleOfGrainHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\LondonDocksHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\PiratesHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\SaltEndHarborBuilder;
use BinaryStudioAcademy\Game\Builder\HarborBuilder\SouthhamptonHarborBuilder;

class CreateHarbors
{
    private $piratesHarbor;
    private $southampton;
    private $saltEnd;
    private $fishguard;
    private $isleOfGrain;
    private $grays;
    private $felixtowe;
    private $londonDocks;
    private $harbors;

    public function createHarbor() : array
    {
        $this->piratesHarbor = (array) (new HarborDirector())->build(new PiratesHarborBuilder(new Harbor()));
        $this->southampton = (array) (new HarborDirector())->build(new SouthhamptonHarborBuilder(new Harbor()));
        $this->saltEnd = (array) (new HarborDirector())->build(new SaltEndHarborBuilder(new Harbor()));
        $this->fishguard = (array) (new HarborDirector())->build(new FishguardHarborBuilder(new Harbor()));
        $this->isleOfGrain = (array) (new HarborDirector())->build(new IsleOfGrainHarborBuilder(new Harbor()));
        $this->grays = (array) (new HarborDirector())->build(new GraysHarborBuilder(new Harbor()));
        $this->felixtowe = (array) (new HarborDirector())->build(new FelixtoweHarborBuilder(new Harbor()));
        $this->londonDocks = (array) (new HarborDirector())->build(new LondonDocksHarborBuilder(new Harbor()));

        return $this->harbors = [
          1 => $this->piratesHarbor,
          2 => $this->southampton,
          3 => $this->saltEnd,
          4 => $this->fishguard,
          5 => $this->isleOfGrain,
          6 => $this->grays,
          7 => $this->felixtowe,
          8 => $this->londonDocks
        ];
    }
}