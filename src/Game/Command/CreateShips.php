<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Builder\ShipBuilder\HMSRoyalSovereignBuilder;
use BinaryStudioAcademy\Game\Builder\ShipBuilder\PirateShipBuilder;
use BinaryStudioAcademy\Game\Builder\ShipBuilder\RoyalBattleShipBuilder;
use BinaryStudioAcademy\Game\Builder\ShipBuilder\RoyalPatroolSchoonerBuilder;
use BinaryStudioAcademy\Game\Builder\ShipBuilder\Ship;
use BinaryStudioAcademy\Game\Builder\ShipBuilder\ShipDirector;

class CreateShips
{
    private $ships;
    private $player;
    private $schooner;
    private $battle;
    private $royal;

    public function createShips() : array
    {
        $this->player = (array) (new ShipDirector())->build(new PirateShipBuilder(new Ship()));
        $this->schooner = (array) (new ShipDirector())->build(new RoyalPatroolSchoonerBuilder(new Ship()));
        $this->battle = (array) (new ShipDirector())->build(new RoyalBattleShipBuilder(new Ship()));
        $this->royal = (array) (new ShipDirector())->build(new HMSRoyalSovereignBuilder(new Ship()));

        return $this->ships = [
            'player' => $this->player,
            'schooner' => $this->schooner,
            'battle' => $this->battle,
            'royal' => $this->royal
            ];
    }
}