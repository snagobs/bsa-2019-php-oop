<?php

namespace BinaryStudioAcademy\Game\Helpers;

class Constants
{
    const PIRATES_HARBOR = 'Pirates Harbor';
    const SOUTHHAMPTON = 'Southhampton';
    const FISHGUARD = 'Fishguard';
    const SALT_END = 'Salt End';
    const ISLE_OF_GRAIN = 'Isle of Grain';
    const GRAYS = 'Grays';
    const FELIXTOWE = 'Felixtowe';
    const LONDON_DOCKS = 'London Docks';
    const PLAYER = 'player';
    const SCHOONER = 'schooner';
    const BATTLE = 'battle';
    const ROYAL = 'royal';
    const PIRATE_SHIP = 'Pirate Ship';
    const ROYAL_PATROOL_SCHOONER = 'Royal Patrool Schooner';
    const ROYAL_BATTLE_SHIP = 'Royal Battle Ship';
    const HMS_ROYAL_SOVEREIGN = 'HMS Royal Sovereign';
}