<?php

require __DIR__ . '/vendor/autoload.php';

$reader = new BinaryStudioAcademy\Game\Io\CliReader;
$writer = new BinaryStudioAcademy\Game\Io\CliWriter;
$random = new BinaryStudioAcademy\Game\Helpers\Random;
$messages = new BinaryStudioAcademy\Game\Command\Messages;
$ships = new BinaryStudioAcademy\Game\Command\CreateShips;
$harbors = new BinaryStudioAcademy\Game\Command\CreateHarbors;

$game = new BinaryStudioAcademy\Game\Game($random);


$game->start($reader, $writer, $messages, $ships, $harbors);
